import React from 'react'
import ReactDOM from 'react-dom'
import Main from './main'

import './main.css'

ReactDOM.render(
  <React.StrictMode>
    <Main />
  </React.StrictMode>,
  document.getElementById('root'),
)

if ('serviceWorker' in navigator && process.env.NODE_ENV) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js')
  })
}
