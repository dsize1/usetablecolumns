type Key = string | number | symbol;

const HeadSymbol = Symbol('head');
const TailSymbol = Symbol('tail');

class Node {
  key: Key;
  next: Node | null;
  prev: Node | null;
  constructor(k: Key) {
    this.key = k;
    this.next = null;
    this.prev = null;
  }
  destroy () {
    this.next = null;
    this.prev = null;
  }
}

interface IteratorResult {
  value: Key | undefined;
  done: boolean;
}

class Iterator {
  point: DoubleListNode;
  constructor(p: DoubleListNode) {
    this.point = p;
  }

  next() {
    const result:IteratorResult = {
      value: undefined,
      done: false
    };
    if(this.point.key !== TailSymbol) {
      result.value = this.point.key;
      this.point = this.point.next as DoubleListNode;
    } else {
      result.done = true;
    }
    return result;
  }
}

interface IHeadNode extends Node {
  next: DoubleListNode | ITailNode;
};

interface ITailNode extends Node {
  prev: DoubleListNode | IHeadNode;
};

interface DoubleListNode extends Node {
  next: DoubleListNode | ITailNode;
  prev: DoubleListNode | IHeadNode;
}

class DoubleList {
  head: Node;
  tail: Node;
  length: number;
  constructor() {
    // 头指针
    this.head = new Node(HeadSymbol);
    // 尾指针
    this.tail = new Node(TailSymbol);
    this.head.next = this.tail;
    this.tail.prev = this.head;
    // 链表长度
    this.length = 0;
  }

  push(key: Key) {
    const node = new Node(key);
    return this._insertNode(node, this.tail.prev as DoubleListNode);
  }

  unshift(key: Key) {
    const node = new Node(key);
    return this._insertNode(node, this.head as DoubleListNode);
  }

  insert(key: Key, prev: DoubleListNode) {
    if (prev.key === TailSymbol) return null;
    const node = new Node(key);
    return this._insertNode(node, prev as DoubleListNode);
  }

  _insertNode(node: Node, prev: DoubleListNode) {
    node.next = prev.next;
    node.prev = prev;
    prev.next = node as DoubleListNode;
    node.next.prev = node as DoubleListNode;
    this.length +=1;
    return node;
  }

  swap(node1: DoubleListNode, node2: DoubleListNode) {
    if (node1.key !== TailSymbol && 
      node1.key !== HeadSymbol &&
      node2.key !== TailSymbol && 
      node2.key !== HeadSymbol
    ) {
      const temp = node1.key;
      node1.key = node2.key;
      node2.key = temp;
    }
  }

  remove(node: DoubleListNode) {
    const prev = node.prev;
    prev.next = node.next;
    node.next.prev = prev;
    this.length -= 1;
    node.destroy();
    return node.key;
  }

  shift() {
    if (this.length > 0)
      return this.remove(this.head.next as DoubleListNode);
    return null
  }

  pop() {
    if (this.length > 0)
      return this.remove(this.tail.prev as DoubleListNode);
    return null
  }

  clear() {
    this.head.next = null;
    this.tail.prev = null;
    this.length = 0;
  }

  [Symbol.iterator]() {
    return new Iterator(this.head.next as DoubleListNode);
  }
};

export default DoubleList;
