import { useReducer, useRef, useEffect } from 'react';
import { isFunction } from 'lodash';

interface IColumn {
  dataIndex: string;
  fixed?: 'left' | 'right';
}

type IsEqualFunc = (prev: IColumn[], next: IColumn[]) => boolean;

type Configs = {
  isEqual?: boolean | IsEqualFunc;
} | undefined;

interface IState {
  columns: IColumn[];
  fixedLeft: React.Key[];
  fixedRight: React.Key[];
  unFixed: React.Key[];
}

interface IInitAction {
  type: 'init',
  payload: {}
}

type AllAction = IInitAction;

const useConstant = <T>(fn: () => T): T => {
  const ref = useRef<{ v: T }>();

  if (!ref.current) {
    ref.current = { v: fn() };
  }

  return ref.current.v;
}

const initializer = (): IState => ({
  columns: [],
  fixedLeft: [],
  fixedRight: [],
  unFixed: [],
});

const reducer = (prevState: IState, action: AllAction) => {
  switch(action.type) {
    case 'init': {
      return initializer();
    }
    default: new Error("Unexpected action: " + action);
  }

  return prevState;
}

const useTableColumns = (
  columns: IColumn[],
  configs: Configs = {}
) => {

  const isInitRef = useRef(false);
  const pervColumns = useRef<IColumn[]>([]);

  const columnsMap = useConstant<Map<string, IColumn>>(() => (new Map()));

  const [state, dispatch] = useReducer(reducer, undefined, initializer);

  useEffect(() => {
    if (
      !isInitRef.current ||
      (isFunction(configs.isEqual) ?
        configs.isEqual(pervColumns.current, columns) :
        configs.isEqual
      )
    ) {
      isInitRef.current = true;
      pervColumns.current = columns;
      columns.forEach((column) => {
        const { dataIndex, fixed } = column;
        columnsMap.set(dataIndex, column);

      });
    }
  }, [columns]);

  useEffect(() => {
    return () => {
      columnsMap.clear();
    };
  })

  return {
    columns: state.columns
  };
};

export default useTableColumns;