const webpack = require('webpack')
const path = require('path')

const entryPath = path.resolve(__dirname, 'testBuild/index.js')
console.log(entryPath)
function test1 (cb) {
  return webpack({
    entry: entryPath,
    mode: 'none',
    output: {
      iife: false,
      pathinfo: 'verbose'
    }
  }, cb)
}

function test2 (cb) {
  return webpack({
    entry: entryPath,
    mode: 'none',
    output: {
      chunkLoading: 'import'
    }
  }, cb)
}

function test3 (cb) {
  return webpack({
    entry: entryPath,
    mode: 'production',
    output: {
      filename: 'main.prd.js'
    }
  }, cb)
}

function test4 (cb) {
  return webpack({
    entry: entryPath,
    mode: 'none',
    output: {
      filename: '[name].[contenthash].js',
      chunkFilename: 'chunk.[name].[id].[contenthash].js',
      path: path.resolve(__dirname, 'dist/deterministic'),
      clean: true
    },
    optimization: {
      moduleIds: 'deterministic',
      chunkIds: 'deterministic'
    }
  }, cb)
}

test1((err, stat) =>　{
  console.log('err', err)
  console.log('stat', stat.toString())
})
