const path = require("path");
// 本插件会将 CSS 提取到单独的文件中，为每个包含 CSS 的 JS 文件创建一个 CSS 文件，并且支持 CSS 和 SourceMaps 的按需加载。
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// 简化了 HTML 文件的创建
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
// terser 来压缩 JavaScript
const TerserWebpackPlugin = require("terser-webpack-plugin");
// 压缩CSS
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// 在一个单独的进程上运行TypeScript类型检查器的Webpack插件
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
// service worker
const WorkboxPlugin = require("workbox-webpack-plugin");

module.exports = function(_env, argv) {
  const isProduction = argv.mode === "production";
  const isDevelopment = !isProduction;

  return {
    // 开发环境cheap-module-source-map
    devtool: isDevelopment && "cheap-module-source-map",
    // 入口
    entry: "./src/index.tsx",
    // 输出
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "assets/js/[name].[contenthash:8].js",
      publicPath: "/"
    },
    // 模块
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              // 指定的目录将用来缓存 loader 的执行结果
              cacheDirectory: true,
              // 当设置此值时，会使用 Gzip 压缩每个 Babel transform 输出
              cacheCompression: false,
              envName: isProduction ? "production" : "development"
            }
          }
        },
        {
          test: /\.css$/,
          exclude: /\.module\.css$/,
          use: [
            isProduction ? MiniCssExtractPlugin.loader : "style-loader",
            "css-loader",
            "postcss-loader"
          ]
        },
        {
          test: /\.worker\.js$/,
          loader: "worker-loader"
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: {
            loader: "url-loader",
            options: {
              limit: 8192,
              name: "static/media/[name].[hash:8].[ext]"
            }
          }
        },
        {
          // svgo 优化
          test: /\.svg$/,
          use: ["@svgr/webpack"]
        },
        {
          test: /\.(eot|otf|ttf|woff|woff2)$/,
          loader: require.resolve("file-loader"),
          options: {
            name: "static/media/[name].[hash:8].[ext]"
          }
        }
      ]
    },
    resolve: {
      extensions: [".js", ".jsx", ".ts", ".tsx"],
      alias: {
        Hooks: path.resolve(__dirname, 'src/hooks'),
      }
    },
    plugins: [
      isProduction
        && new MiniCssExtractPlugin({
          filename: "assets/css/[name].[contenthash:8].css",
          chunkFilename: "assets/css/[name].[contenthash:8].chunk.css"
        }),
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "public/index.html"),
        inject: true
      }),
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify(
          isProduction ? "production" : "development"
        )
      }),
      new ForkTsCheckerWebpackPlugin({
        async: false // 防止Webpack在运行开发服务器时发出无效代码，并在覆盖层中显示编译错误。
      }),
      new WorkboxPlugin.GenerateSW({
        swDest: "service-worker.js",
        clientsClaim: true,
        skipWaiting: true
      })
    ].filter(Boolean),
    optimization: {
      minimize: isProduction,
      minimizer: [
        new TerserWebpackPlugin({
          terserOptions: {
            compress: {
              comparisons: false
            },
            mangle: {
              safari10: true
            },
            output: {
              comments: false,
              ascii_only: true
            },
            warnings: false
          }
        }),
        // new OptimizeCssAssetsPlugin()
      ],
      splitChunks: {
        chunks: "all",
        // 生成块的最小大小(以字节为单位)
        minSize: 0,
        // 入口点的最大并行请求数
        maxInitialRequests: 10,
        // 按需加载时的最大并行请求数
        maxAsyncRequests: 10,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name(module, chunks, cacheGroupKey) {
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1];
              return `${cacheGroupKey}.${packageName.replace("@", "")}`;
            }
          },
          common: {
            minChunks: 2,
            priority: -10
          }
        }
      },
      // 值 "single" 会创建一个在所有生成 chunk 之间共享的运行时文件
      runtimeChunk: "single"
    },
    devServer: {
      compress: true,
      historyApiFallback: true,
      open: true,
      // client: {
      //   overlay: true
      // }
    }
  };
};