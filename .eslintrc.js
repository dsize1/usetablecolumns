module.exports = {
  // 指定 ESLint parser
  parser: '@typescript-eslint/parser',
  extends: [
    // 使用 @eslint-plugin-react 中推荐的规则
    'plugin:react/recommended',
    // 使用 @typescript-eslint/eslint-plugin 中推荐的规则
    'plugin:@typescript-eslint/recommended',
    // 使用 eslint-config-prettier 来禁止 @typescript-eslint/eslint-plugin 中那些和 prettier 冲突的规则
    'prettier/@typescript-eslint',
    // 使用 eslint-plugin-prettier 来将 prettier 错误作为 ESLint 错误显示
    // 确保下面这行配置是这个数组里的最后一行配置
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2018, // 允许解析现代 es 特性
    sourceType: 'module', // 允许使用 imports
    ecmaFeatures: {
      jsx: true, // 允许解析 jsx
    },
  },
  rules: {
    'react/display-name': 'off',
    'react/prop-types': 'off',
    'import/no-unresolved': 'off',
    'react/jsx-filename-extension': [1, { "extensions": [".ts", ".tsx"] }],
    'no-undef': 'off',
    // 类名与接口名必须为驼峰式
    '@typescript-eslint/class-name-casing': 'error',
  },
  settings: {
    react: {
      // Tells eslint-plugin-react to automatically detect the version of React to use
      version: 'detect',
    },
  },
  // eslintIgnore: ['tsconfig.json']
}